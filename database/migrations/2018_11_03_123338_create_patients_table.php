<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('patients', function (Blueprint $table) {
      $table->increments('id');
      $table->string('patient_reference');
      $table->string('patient_id');
      $table->string('last_name');
      $table->string('first_name');
      $table->date('dob');
      $table->string('nationality')->nullable();
      $table->string('center')->nullable();
      $table->string('gender')->comment('1 for Female,2 for Male');
      $table->integer('age')->nullable();
      $table->enum('approved_consent',[0,1])->default(0);
      $table->date('consent_date');
      $table->string('consent_file')->nullable();
      $table->string('mutation1')->nullable();
      $table->string('mutation2')->nullable();
      $table->enum('neonatal_screening',[0,1])->default(0);
      $table->string('sweat_test1')->nullable();
      $table->enum('meconium',[0,1])->default(0);
      $table->string('additional_diagnosis')->nullable();
      $table->enum('faecal_elastase',[0,1])->default(0);
      $table->string('irt1')->nullable();
      $table->string('irt2')->nullable();
      $table->string('sweat_test2')->nullable();
      $table->enum('surgery',[0,1])->default(0);
      $table->enum('diagnosis_confirmed',[0,1])->default(0);
      $table->date('faecal_elastase_date');
      $table->enum('pancreatic_status_pl',[0,1])->default(0);
      $table->enum('pancreatic_status_ps',[0,1])->default(0);
      $table->date('date_of_death');
      $table->longText('cause_of_death')->nullable();
      $table->enum('status',[0,1])->default(1);
      $table->string('added_by');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('patients');
  }
}
