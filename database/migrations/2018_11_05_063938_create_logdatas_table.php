<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logdata', function (Blueprint $table) {
          $table->increments('id');
          $table->string('log_reference');
          $table->string('itemId');
          $table->string('action');
          $table->string('type');
          $table->date('added_date');
          $table->time('added_time');
          $table->string('added_by');
          $table->enum('status',[0,1])->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logdata');
    }
}
