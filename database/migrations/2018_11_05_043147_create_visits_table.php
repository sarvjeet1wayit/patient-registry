<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('visit_reference');
            $table->string('patient_reference');
            $table->date('follow_up_date');
            $table->string('age_at_follow_up');
            $table->enum('hospitalization',[0,1])->default(0);
            $table->integer('weight')->nullable();
            $table->integer('height')->nullable();
            $table->string('bmi')->nullable();
            $table->integer('weight_z_score')->nullable();
            $table->integer('height_z_score')->nullable();
            $table->integer('bmi_z_score')->nullable();
            $table->date('fev_1_date');
            $table->string('value_fev_1')->nullable();
            $table->decimal('fev_1_percentage', 5, 2)->nullable();
            $table->string('value_fev')->nullable();
            $table->decimal('fev_percentage', 5, 2)->nullable();
            $table->string('age_at_fev_1')->nullable();
            $table->enum('pseudom_aeruginosa',[0,1])->default(0);
            $table->enum('staphylococcus_aureus',[0,1])->default(0);
            $table->enum('burkh_cepacia',[0,1])->default(0);
            $table->enum('non_tuberculous_mycobacteria',[0,1])->default(0);
            $table->enum('stenotroph_maltophilia',[0,1])->default(0);
            $table->enum('abpa',[0,1])->default(0);
            $table->enum('diabetes',[0,1])->default(0);
            $table->enum('use_insulin',[0,1])->default(0);
            $table->enum('pneumothorax',[0,1])->default(0);
            $table->enum('liver_disease',[0,1])->default(0);
            $table->integer('select_dropdown_1')->nullable();
            $table->enum('hemoptysis',[0,1])->default(0);
            $table->string('dios')->nullable();
            $table->enum('osteoporosis',[0,1])->default(0);
            $table->enum('dexa',[0,1])->default(0);
            $table->enum('nasal_polyps',[0,1])->default(0);
            $table->enum('occur_malignancy',[0,1])->default(0);
            $table->enum('liver_transp',[0,1])->default(0);
            $table->year('liver_transp_year')->nullable();
            $table->enum('lung_transp',[0,1])->default(0);
            $table->year('lung_transp_year')->nullable();
            $table->integer('ultrasound')->nullable();
            $table->integer('chest_ct')->nullable();
            $table->enum('inhaled_hypertonnic_naci',[0,1])->default(0);
            $table->integer('antibiotics_inhaled')->nullable();
            $table->enum('bronchodil',[0,1])->default(0);
            $table->enum('oxygen',[0,1])->default(0);
            $table->enum('rhDNase',[0,1])->default(0);
            $table->enum('ursodeoxyc',[0,1])->default(0);
            $table->enum('use_enzymes',[0,1])->default(0);
            $table->enum('vitamins',[0,1])->default(0);
            $table->enum('trebon',[0,1])->default(0);
            $table->enum('prophylaxis_with_augmentinn',[0,1])->default(0);
            $table->enum('azithromycin',[0,1])->default(0);
            $table->enum('correctors_potemtoatprs',[0,1])->default(0);
            $table->integer('select_dropdown_2')->nullable();
            $table->enum('ranitidine',[0,1])->default(0);
            $table->enum('omeprazole',[0,1])->default(0);
            $table->enum('duphalac',[0,1])->default(0);
            $table->text('others')->nullable();
            $table->enum('status',[0,1])->default(1);
            $table->string('added_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
