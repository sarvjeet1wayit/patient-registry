<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          'category_ref' => '1',
          'category_name' => 'Demographics',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '2',
        'category_name' => 'Diagnosis',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '3',
        'category_name' => 'Genotype',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '4',
        'category_name' => 'Lung Function',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '5',
        'category_name' => 'Microbiology',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '6',
        'category_name' => 'Nutrition',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '7',
        'category_name' => 'Complications',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '8',
        'category_name' => 'Therapy',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '9',
        'category_name' => 'Transplants',
        'created_at' => now(),
        'updated_at' => now()
      ]);
      DB::table('categories')->insert([
        'category_ref' => '10',
        'category_name' => 'Survival',
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }
}
