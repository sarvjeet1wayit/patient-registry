<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_reference' => 'superadmin',
            'name' => 'Super Admin',
            'email' => 'admin@mdata.com',
            'password' => bcrypt('admin786'),
            'created_at'  =>now(),
            'updated_at' => now()
        ]);
    }
}
