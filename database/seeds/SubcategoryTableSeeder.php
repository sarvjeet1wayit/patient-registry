<?php

use Illuminate\Database\Seeder;

class SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('subcategories')->insert([
          'subcategory_ref' => '1',
          'parent_ref' => '3',
          'subcategory_name' => 'Mutation Categories Distribution',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('subcategories')->insert([
          'subcategory_ref' => '2',
          'parent_ref' => '3',
          'subcategory_name' => 'Mutation Class Status by Age',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('subcategories')->insert([
          'subcategory_ref' => '3',
          'parent_ref' => '3',
          'subcategory_name' => 'Patients with 1 mutation/2 mutations',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('subcategories')->insert([
          'subcategory_ref' => '4',
          'parent_ref' => '3',
          'subcategory_name' => 'F508del Mutation Prevalence',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('subcategories')->insert([
          'subcategory_ref' => '5',
          'parent_ref' => '3',
          'subcategory_name' => 'Mutation distribution of patients, by age group',
          'created_at' => now(),
          'updated_at' => now()
      ]);
      DB::table('subcategories')->insert([
          'subcategory_ref' => '6',
          'parent_ref' => '3',
          'subcategory_name' => 'Sweat Chloride Value (mmol/L) by Mutation Class',
          'created_at' => now(),
          'updated_at' => now()
      ]);
    }
}
